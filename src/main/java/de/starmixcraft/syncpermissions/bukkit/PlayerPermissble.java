package de.starmixcraft.syncpermissions.bukkit;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissibleBase;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.ServerOperator;

import de.starmixcraft.syncpermissions.global.PlayerPermissionsData;

public class PlayerPermissble extends PermissibleBase{
	private UUID uuid;
	
	public PlayerPermissble(Player opable) {
		super((ServerOperator)opable);
		this.uuid = opable.getUniqueId();
		
	}
	
	@Override
	public boolean isPermissionSet(Permission perm) {
		return this.hasPermission(perm.getName());
	}
	
	@Override
	public boolean isPermissionSet(String name) {
		return this.hasPermission(name);
	}
	
	@Override
	public boolean hasPermission(Permission perm) {
		 return this.hasPermission(perm.getName());
	}
	@Override
	public boolean hasPermission(String inName) {
        if (inName.equalsIgnoreCase("bukkit.broadcast.user")) {
            return true;
        }
        PlayerPermissionsData data = BukkitMain.getBukkitPermissionsManager().get(uuid);     
        if(data == null) {
        	return false;
        }
        if(data.isHasAdmin()) {
        	return true;
        }
        return data.getPermissions().contains(inName);
	}
}
