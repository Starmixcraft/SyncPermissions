package de.starmixcraft.syncpermissions.bukkit;

import java.util.UUID;

import de.starmixcraft.syncpermissions.global.ByteBufSerelizer;
import de.starmixcraft.syncpermissions.global.PlayerPermissionsData;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

import java.util.HashMap;

public class BukkitPermissionsManager {
	private HashMap<UUID, PlayerPermissionsData> perms;
	
	public BukkitPermissionsManager() {
		perms = new HashMap<>();
	}
	
	
	
	
	public PlayerPermissionsData load(byte[] data) {
		ByteBuf buf = ByteBufAllocator.DEFAULT.heapBuffer();
		buf.writeBytes(data);
		ByteBufSerelizer bufSerelizer = new ByteBufSerelizer(buf);
		UUID uuid = bufSerelizer.readUUID();
		PlayerPermissionsData playerPermissionsData = new PlayerPermissionsData();
		playerPermissionsData.read(bufSerelizer);
		playerPermissionsData.setUuid(uuid);
		perms.put(uuid, playerPermissionsData);
		buf.release();
		return playerPermissionsData;
	}




	public PlayerPermissionsData get(UUID key) {
		return perms.get(key);
	}
	
	
	public void remove(UUID key) {
		perms.remove(key);
	}

	
}
