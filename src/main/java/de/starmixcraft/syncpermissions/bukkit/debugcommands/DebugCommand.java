package de.starmixcraft.syncpermissions.bukkit.debugcommands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import de.starmixcraft.syncpermissions.Conf;

public class DebugCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof ConsoleCommandSender) {
			return false;
		}
		if(!Conf.isDebug()) {
			return false;
		}
		Player p = (Player) sender;
		if(args.length == 0) {
			p.sendMessage("/debug <permissions>");
			return false;
		}
		p.sendMessage(Conf.getPr() + " value for permission " + args[0] +":"+ p.hasPermission(args[0]));
		return false;
	}

}
