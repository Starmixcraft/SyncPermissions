package de.starmixcraft.syncpermissions.bukkit.pluginchannel;

import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import de.starmixcraft.syncpermissions.Conf;
import de.starmixcraft.syncpermissions.bukkit.BukkitMain;
import de.starmixcraft.syncpermissions.global.PlayerPermissionsData;

public class ChannelListener implements PluginMessageListener{

	@Override
	public void onPluginMessageReceived(String channel, Player reciver, byte[] data) {
	if(channel.equalsIgnoreCase("syncpermissions:sync")) {
		PlayerPermissionsData permissionsData = BukkitMain.getBukkitPermissionsManager().load(data);
		if(Conf.isDebug()) {
			System.out.println(Conf.getPr() + "Recived data for " + permissionsData.getUuid());
		}
	}
	}



}
