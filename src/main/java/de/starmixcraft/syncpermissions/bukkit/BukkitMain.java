package de.starmixcraft.syncpermissions.bukkit;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import de.starmixcraft.syncpermissions.bukkit.debugcommands.DebugCommand;
import de.starmixcraft.syncpermissions.bukkit.listener.PlayerLoginListener;
import de.starmixcraft.syncpermissions.bukkit.listener.PlayerQuitListener;
import de.starmixcraft.syncpermissions.bukkit.pluginchannel.ChannelListener;
import lombok.Getter;

public class BukkitMain extends JavaPlugin{
	@Getter
	private static BukkitPermissionsManager bukkitPermissionsManager;
	
	
	@Override
	public void onEnable() {
		bukkitPermissionsManager = new BukkitPermissionsManager();
		Bukkit.getMessenger().registerIncomingPluginChannel(this, "syncpermissions:sync", new ChannelListener());
		Bukkit.getPluginManager().registerEvents(new PlayerLoginListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerQuitListener(), this);
		
		getCommand("debug").setExecutor(new DebugCommand());
	}
}
