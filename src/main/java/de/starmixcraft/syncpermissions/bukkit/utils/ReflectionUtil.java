package de.starmixcraft.syncpermissions.bukkit.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Wither;

public class ReflectionUtil {
	public static Class<?> reflectCraftClazz(final String suffix) {
        try {
            final String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
            return Class.forName("org.bukkit.craftbukkit." + version + suffix);
        }
        catch (Exception ex) {
            try {
                return Class.forName("org.bukkit.craftbukkit." + suffix);
            }
            catch (ClassNotFoundException ex2) {
                return null;
            }
        }
    }
    
    public static Class<?> forName(final String path) {
        try {
            return Class.forName(path);
        }
        catch (ClassNotFoundException e) {
            return null;
        }
    }
    
    public static Class<?> reflectNMSClazz(final String suffix) {
        try {
            final String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
            return Class.forName("net.minecraft.server." + version + suffix);
        }
        catch (Exception ex) {
            try {
                return Class.forName("net.minecraft.server." + suffix);
            }
            catch (ClassNotFoundException ex2) {
                return null;
            }
        }
    }
    
}
