package de.starmixcraft.syncpermissions.bukkit.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import de.starmixcraft.syncpermissions.bukkit.BukkitMain;

public class PlayerQuitListener implements Listener{
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		BukkitMain.getBukkitPermissionsManager().remove(e.getPlayer().getUniqueId());
	}

}
