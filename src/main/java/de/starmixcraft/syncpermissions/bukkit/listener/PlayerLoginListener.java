package de.starmixcraft.syncpermissions.bukkit.listener;

import java.lang.reflect.Field;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import de.starmixcraft.syncpermissions.bukkit.PlayerPermissble;
import de.starmixcraft.syncpermissions.bukkit.utils.ReflectionUtil;

public class PlayerLoginListener implements Listener{

	@EventHandler
	public void onLogin(PlayerLoginEvent e) {
        try {
            final Class<?> clazz = ReflectionUtil.reflectCraftClazz(".entity.CraftHumanEntity");
            Field field;
            if (clazz != null) {
                field = clazz.getDeclaredField("perm");
            }
            else {
                field = Class.forName("net.glowstone.entity.GlowHumanEntity").getDeclaredField("permissions");
            }
            field.setAccessible(true);
            field.set(e.getPlayer(), new PlayerPermissble(e.getPlayer()));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
	}
}
