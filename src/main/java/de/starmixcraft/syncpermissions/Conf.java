package de.starmixcraft.syncpermissions;

import lombok.Getter;

public class Conf {
	@Getter
	private static boolean debug = false;
	@Getter
	private static String pr = "[SyncPermissions] > ";
}
