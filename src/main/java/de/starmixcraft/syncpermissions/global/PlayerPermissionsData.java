package de.starmixcraft.syncpermissions.global;

import java.util.ArrayList;
import java.util.UUID;

import de.starmixcraft.syncpermissions.bungee.BungeeMain;
import de.starmixcraft.syncpermissions.bungee.permissions.DataBasePlayerData;
import de.starmixcraft.syncpermissions.bungee.permissions.PermissionsGroup;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PlayerPermissionsData {
	private ArrayList<String> permissions;
	private UUID uuid;
	private DataBasePlayerData basePlayerData;
	private boolean hasAdmin;
	
	
	
	
	public void recal() {
		permissions.clear();
		PermissionsGroup group = BungeeMain.getPerissionsManager().getGroup(basePlayerData.getGroup());
		permissions.addAll(group.getPermissionsWithHeritages());
		this.hasAdmin = group.isAdmin();
		permissions.addAll(basePlayerData.getPermissions());
	}
	
	public void write(ByteBufSerelizer serelizer) {
		serelizer.writeBoolean(hasAdmin);
		serelizer.writeVarInt(permissions.size());
		for (String s : permissions) {
			serelizer.writeString(s);
		}
	}
	
	
	
	public void read(ByteBufSerelizer serelizer) {
		this.hasAdmin = serelizer.readBoolean();
		int stringstoread = serelizer.readVarInt();
		permissions = new ArrayList<>();
		for (int i = 0; i < stringstoread; i++) {
			permissions.add(serelizer.readString());
		}
	}
}
