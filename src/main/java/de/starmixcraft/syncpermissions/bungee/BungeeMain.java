package de.starmixcraft.syncpermissions.bungee;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.starmixcraft.syncpermissions.bungee.commands.SyncPermCommand;
import de.starmixcraft.syncpermissions.bungee.listeners.LoginListener;
import de.starmixcraft.syncpermissions.bungee.listeners.PermListener;
import de.starmixcraft.syncpermissions.bungee.listeners.QuitListener;
import de.starmixcraft.syncpermissions.bungee.listeners.ServerConnectListener;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

public class BungeeMain extends Plugin{
	@Getter
	private static BungeeMain instance;
	@Getter
	private static final Gson prittygson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().enableComplexMapKeySerialization().create();
	@Getter
	private static PermissionsManager perissionsManager;
	@Getter
	private static NameToUUIDMapping nameToUUIDMapping;
	
	
	@Override
	public void onEnable() {
		instance = this;
		System.out.println("Loading SyncPermissions...");
		long start = System.currentTimeMillis();
		if(!getDataFolder().exists()) {
			getDataFolder().mkdirs();
		}
		nameToUUIDMapping = new NameToUUIDMapping();
		perissionsManager = new PermissionsManager();
		ProxyServer.getInstance().registerChannel("SyncPermissions:sync");
		ProxyServer.getInstance().getPluginManager().registerListener(instance, new LoginListener());
		ProxyServer.getInstance().getPluginManager().registerListener(instance, new PermListener());
		ProxyServer.getInstance().getPluginManager().registerListener(instance, new QuitListener());
		ProxyServer.getInstance().getPluginManager().registerListener(instance, new ServerConnectListener());
		ProxyServer.getInstance().getPluginManager().registerListener(instance, nameToUUIDMapping);
		
		ProxyServer.getInstance().getPluginManager().registerCommand(instance, new SyncPermCommand());
		System.out.println("Loaded SyncPermissions in " + (System.currentTimeMillis() - start)  + " Miliseconds");
		
	}
	
	
	@Override
	public void onDisable() {
	nameToUUIDMapping.save();
	}
}
