package de.starmixcraft.syncpermissions.bungee.listeners;

import de.starmixcraft.syncpermissions.bungee.BungeeMain;
import de.starmixcraft.syncpermissions.global.PlayerPermissionsData;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class QuitListener implements Listener{

	
	@EventHandler
	public void onQuit(PlayerDisconnectEvent e) {
		PlayerPermissionsData player =	BungeeMain.getPerissionsManager().removePermData(e.getPlayer().getUniqueId());
		BungeeMain.getPerissionsManager().saveData(e.getPlayer().getUniqueId(), player.getBasePlayerData());
	}
}
