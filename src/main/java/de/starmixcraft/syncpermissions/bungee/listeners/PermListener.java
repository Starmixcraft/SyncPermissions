package de.starmixcraft.syncpermissions.bungee.listeners;


import de.starmixcraft.syncpermissions.bungee.BungeeMain;
import de.starmixcraft.syncpermissions.global.PlayerPermissionsData;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PermissionCheckEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PermListener implements Listener{
	
	@EventHandler
	public void permEvent(PermissionCheckEvent e) {
		ProxiedPlayer player = (ProxiedPlayer) e.getSender();
		PlayerPermissionsData data = BungeeMain.getPerissionsManager().getPermData(player.getUniqueId());
		if(data.isHasAdmin()) {
			e.setHasPermission(true);
			return;
		}
		e.setHasPermission(data.getPermissions().contains(e.getPermission()));
	}

}
