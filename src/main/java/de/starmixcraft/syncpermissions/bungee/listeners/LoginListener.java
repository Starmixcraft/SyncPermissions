package de.starmixcraft.syncpermissions.bungee.listeners;

import java.util.ArrayList;

import de.starmixcraft.syncpermissions.bungee.BungeeMain;
import de.starmixcraft.syncpermissions.bungee.permissions.DataBasePlayerData;
import de.starmixcraft.syncpermissions.bungee.permissions.PermissionsGroup;
import de.starmixcraft.syncpermissions.global.PlayerPermissionsData;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class LoginListener implements Listener{

	@EventHandler
	public void onPreLogin(LoginEvent e) {
	DataBasePlayerData basePlayerData = BungeeMain.getPerissionsManager().getData(e.getConnection().getUniqueId());
	PlayerPermissionsData permissionsData = new PlayerPermissionsData(new ArrayList<>(),e.getConnection().getUniqueId(), basePlayerData, false);
	permissionsData.recal();
	BungeeMain.getPerissionsManager().addDataToList(e.getConnection().getUniqueId(), permissionsData);
	}
	
}
