package de.starmixcraft.syncpermissions.bungee.listeners;

import de.starmixcraft.syncpermissions.bungee.BungeeMain;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ServerConnectListener implements Listener{
	@EventHandler
	public void onConnect(ServerConnectedEvent e) {
		e.getServer().sendData("syncpermissions:sync", BungeeMain.getPerissionsManager().getPermDataAsByteArray(e.getPlayer().getUniqueId()));
	}

}
