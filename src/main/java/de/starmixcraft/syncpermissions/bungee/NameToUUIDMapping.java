package de.starmixcraft.syncpermissions.bungee;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.UUID;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.google.common.reflect.TypeToken;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class NameToUUIDMapping implements Listener{
	private ArrayList<MapEntry> map;
	private static File datafile;
	
	public NameToUUIDMapping() {
		map = new ArrayList<>();
		datafile = new File(BungeeMain.getInstance().getDataFolder(), "data.do_not_edit");
		load();
	}
	
	
	public void load() {
		if(datafile.exists()) {
			try {
			InputStreamReader gzipInputStream = new InputStreamReader(new GZIPInputStream(new FileInputStream(datafile)));
			map = BungeeMain.getPrittygson().fromJson(gzipInputStream, new TypeToken<ArrayList<MapEntry>>() {
			}.getType());
			gzipInputStream.close();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void save() {
		try {
		OutputStreamWriter gzipInputStream = new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(datafile)));
		gzipInputStream.write(BungeeMain.getPrittygson().toJson(this));
		gzipInputStream.flush();
		gzipInputStream.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private MapEntry getEntry(UUID uuid) {
		for(MapEntry entry : map) {
			if(entry.getUuid().equals(uuid)) {
				return entry;
			}
		}
		return null;
	}
	
	private MapEntry getEntry(String name) {
		for(MapEntry entry : map) {
			if(entry.getName().equals(name)) {
				return entry;
			}
		}
		return null;
	}
	
	
	public String getName(UUID uuid) {
		MapEntry entry = getEntry(uuid);
		if(entry == null) {
			return null;
		}
		return entry.getName();
	}
	
	public UUID getUUID(String name) {
		MapEntry entry = getEntry(name);
		if(entry == null) {
			return null;
		}
		return entry.getUuid();
	}
	
	
	private void updateMapEntry(ProxiedPlayer p) {
		MapEntry entry = getEntry(p.getUniqueId());
		if(entry == null) {
			entry = new MapEntry(p.getName(), p.getUniqueId());
			map.add(entry);
			return;
		}
		entry.setName(p.getName());
	}
	
	@EventHandler
	public void onLogin(PostLoginEvent e) {
		updateMapEntry(e.getPlayer());
	}
	
	
	@AllArgsConstructor
	@Getter
	private static class MapEntry{
		@Setter
		private String name;
		private UUID uuid;
	}
}
