package de.starmixcraft.syncpermissions.bungee.commands;

import java.util.UUID;

import de.starmixcraft.syncpermissions.Conf;
import de.starmixcraft.syncpermissions.bungee.BungeeMain;
import de.starmixcraft.syncpermissions.bungee.permissions.DataBasePlayerData;
import de.starmixcraft.syncpermissions.bungee.permissions.PermissionsGroup;
import de.starmixcraft.syncpermissions.global.PlayerPermissionsData;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class SyncPermCommand extends Command{

	
	/*
	 *  /syncperm setgroup <Player> <Group>
	 *  /syncperm addpermission <Player> <permission>
	 *  /syncperm removepermission <Player> <permission>
	 */
	public SyncPermCommand() {
		super("SyncPerm");
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if(!sender.hasPermission("Syncperm.admin")) {
			return;
		}
		if(args.length == 3) {
			String name = args[1];
			String parm = args[2];
			String action = args[0];
			
			UUID targetuuid = null;
			ProxiedPlayer player = ProxyServer.getInstance().getPlayer(name);
			boolean online = false;
			if(player != null) {
				targetuuid = player.getUniqueId();
				online = true;
			}else {
				UUID saveuuid = BungeeMain.getNameToUUIDMapping().getUUID(name);
				if(saveuuid == null) {
					sender.sendMessage(Conf.getPr() + "Unable to find uuid of player " + name);
					return;
				}
				targetuuid = saveuuid;
			}
			
			if(action.equalsIgnoreCase("setgroup")) {
				PermissionsGroup group = BungeeMain.getPerissionsManager().getGroup(parm);
				if(group == null) {
					sender.sendMessage(Conf.getPr() + "Group was not found!");
					return;
				}
				if(online) {
				PlayerPermissionsData data = BungeeMain.getPerissionsManager().getPermData(targetuuid);
				data.getBasePlayerData().setGroup(group.getName());
				data.recal();
				sendPermUpdate(player);
				}else {
					DataBasePlayerData basePlayerData = BungeeMain.getPerissionsManager().getData(targetuuid);
					basePlayerData.setGroup(group.getName());
					BungeeMain.getPerissionsManager().saveData(targetuuid, basePlayerData);
				}
				
			}else
			if(action.equalsIgnoreCase("addpermission")) {
				DataBasePlayerData basePlayerData = null;
				if(online) {
					basePlayerData = BungeeMain.getPerissionsManager().getPermData(targetuuid).getBasePlayerData();
				}else {
					basePlayerData = BungeeMain.getPerissionsManager().getData(targetuuid);
				}
				
				if(basePlayerData.getPermissions().contains(parm)) {
					sender.sendMessage(Conf.getPr() + "The player allready has that permission!");
					return;
				}
				basePlayerData.getPermissions().add(parm);
				
				if(online) {
					PlayerPermissionsData data = BungeeMain.getPerissionsManager().getPermData(targetuuid);
					data.setBasePlayerData(basePlayerData);
					data.recal();
					sendPermUpdate(player);
				}else {
					BungeeMain.getPerissionsManager().saveData(targetuuid, basePlayerData);
				}
			}else
			if(action.equalsIgnoreCase("removepermission")) {
				DataBasePlayerData basePlayerData = null;
				if(online) {
					basePlayerData = BungeeMain.getPerissionsManager().getPermData(targetuuid).getBasePlayerData();
				}else {
					basePlayerData = BungeeMain.getPerissionsManager().getData(targetuuid);
				}
				
				if(!basePlayerData.getPermissions().contains(parm)) {
					sender.sendMessage(Conf.getPr() + "The player dosent have the permission!");
					return;
				}
				basePlayerData.getPermissions().add(parm);
				
				if(online) {
					PlayerPermissionsData data = BungeeMain.getPerissionsManager().getPermData(targetuuid);
					data.setBasePlayerData(basePlayerData);
					data.recal();
					sendPermUpdate(player);
				}else {
					BungeeMain.getPerissionsManager().saveData(targetuuid, basePlayerData);
				}
			}else {
				sendHelp(sender);
			}
		}else {
			sendHelp(sender);
		}
	}
	
	
	
	@SuppressWarnings("deprecation")
	public void sendHelp(CommandSender sender) {
		sender.sendMessage("----SyncPerm Command Help----");
		sender.sendMessage("/syncperm setgroup <user> <group>");
		sender.sendMessage("/syncperm addpermission <user> <permission>");
		sender.sendMessage("/syncperm removepermission <user> <permission>");
	}
	
	
	private void sendPermUpdate(ProxiedPlayer player) {
		player.getServer().sendData("syncpermissions:sync", BungeeMain.getPerissionsManager().getPermDataAsByteArray(player.getUniqueId()));
	}

}
