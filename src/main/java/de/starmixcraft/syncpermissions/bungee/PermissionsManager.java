package de.starmixcraft.syncpermissions.bungee;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.google.common.reflect.TypeToken;

import de.starmixcraft.syncpermissions.bungee.permissions.DataBasePlayerData;
import de.starmixcraft.syncpermissions.bungee.permissions.PermissionsGroup;
import de.starmixcraft.syncpermissions.global.ByteBufSerelizer;
import de.starmixcraft.syncpermissions.global.PlayerPermissionsData;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.buffer.Unpooled;

public class PermissionsManager {
	private ArrayList<PermissionsGroup> permissionsGroups;
	private HashMap<UUID, PlayerPermissionsData> perms;
	private static File permfile;
	private static File databasefolder;
	private PermissionsGroup defaultgroup;

	public PermissionsManager() {
		permissionsGroups = new ArrayList<>();
		perms = new HashMap<>();
		permfile = new File(BungeeMain.getInstance().getDataFolder(), "perms.json");
		databasefolder = new File(BungeeMain.getInstance().getDataFolder(), "database");
		if(!databasefolder.exists()) {
			databasefolder.mkdirs();
		}
		load();
	}

	public void save() {
		try {
			FileWriter fileWriter = new FileWriter(permfile);
			fileWriter.write(BungeeMain.getPrittygson().toJson(permissionsGroups));
			fileWriter.flush();
			fileWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("serial")
	public void load() {
		if (permfile.exists()) {
			try {
				FileReader fileReader = new FileReader(permfile);
				permissionsGroups = BungeeMain.getPrittygson().fromJson(fileReader,
						new TypeToken<ArrayList<PermissionsGroup>>() {
						}.getType());
				fileReader.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			performChecks();
			loadGroups();		
		} else {
			createExapleGroups();
			save();
			performChecks();
			loadGroups();	
		}
	}

	private void performChecks() {
		int defaultcount = 0;
		for (PermissionsGroup group : permissionsGroups) {
			if (group.isDefaultgroup()) {
				defaultcount++;
			}
		}
		if (defaultcount == 0) {
			throw new Error("No default group found!");
		}
		if (defaultcount >= 2) {
			throw new Error("2 or more default groups found!");
		}

	}
	
	private void loadGroups() {
		for(PermissionsGroup group : permissionsGroups) {
			loadGroup(group);
			if(group.isDefaultgroup()) {
				defaultgroup = group;
			}
		}
	}

	private void loadGroup(PermissionsGroup permissionsGroup) {
		if (permissionsGroup.isLoaded()) {
			return;
		}
		for (String loadbefore : permissionsGroup.getHeritages()) {
			PermissionsGroup toload = getGroup(loadbefore);
			if (toload == null) {
				throw new Error(
						"The group " + permissionsGroup.getName() + " has a heritage group that is not registed");
			}
			if (toload.getHeritages().contains(permissionsGroup.getName())) {
				throw new Error("Circular heritage detected, the group " + permissionsGroup.getName() + " has heritage "
						+ toload.getName() + " witch has heritage " + permissionsGroup.getName());
			}
			loadGroup(toload);
		}

		permissionsGroup.setPermissionsWithHeritages(new ArrayList<>());
		permissionsGroup.getPermissionsWithHeritages().addAll(permissionsGroup.getPermissions());
		for (String loadedgroups : permissionsGroup.getHeritages()) {
			PermissionsGroup toload = getGroup(loadedgroups);
			permissionsGroup.getPermissionsWithHeritages().addAll(toload.getPermissions());
		}
		permissionsGroup.setLoaded(true);
	}

	public PermissionsGroup getGroup(String name) {
		for (PermissionsGroup group : permissionsGroups) {
			if (group.getName().equals(name))
				return group;
		}
		return null;
	}
	
	public PlayerPermissionsData getPermData(UUID uuid) {
		return perms.get(uuid);
	}
	
	
	public PlayerPermissionsData removePermData(UUID uuid) {
		return perms.remove(uuid);
	}

	public void addDataToList(UUID uuid, PlayerPermissionsData permissionsData) {
		perms.put(uuid, permissionsData);
	}
	
	private void createExapleGroups() {
		ArrayList<String> permsuser = new ArrayList<>();
		permsuser.add("system.user");
		permsuser.add("bungee.user");
		permissionsGroups.add(new PermissionsGroup("User", true, false, permsuser, new ArrayList<>()));
		ArrayList<String> permsvip = new ArrayList<>();
		permsvip.add("system.VIP");
		permsvip.add("bungee.VIP");
		ArrayList<String> erben = new ArrayList<>();
		erben.add("User");
		permissionsGroups.add(new PermissionsGroup("VIP", false, false, permsvip, erben));
		permissionsGroups.add(new PermissionsGroup("Admin", false, true, new ArrayList<>(), new ArrayList<>()));
	}
	
	public void saveData(UUID uuid, DataBasePlayerData dataBasePlayerData) {
		File playerfile = new File(databasefolder, uuid.toString() + ".SPerm");
		try {
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(playerfile)));
			outputStreamWriter.write(BungeeMain.getPrittygson().toJson(dataBasePlayerData));
			outputStreamWriter.flush();
			outputStreamWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public DataBasePlayerData getData(UUID uuid) {
		File playerfile = new File(databasefolder, uuid.toString() + ".SPerm");
		DataBasePlayerData basePlayerData = new DataBasePlayerData(uuid, defaultgroup.getName(), new ArrayList<>());
		if(!playerfile.exists()) {
			return basePlayerData;
		}
		try {
			InputStreamReader gzipInputStream = new InputStreamReader(new GZIPInputStream(new FileInputStream(playerfile)));
			basePlayerData = BungeeMain.getPrittygson().fromJson(gzipInputStream, DataBasePlayerData.class);
			gzipInputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return basePlayerData;
	}
	
	
	public byte[] getPermDataAsByteArray(UUID uuid) {
		ByteBufSerelizer bufSerelizer = new ByteBufSerelizer(Unpooled.buffer());
		bufSerelizer.WriteUUID(uuid);
		getPermData(uuid).write(bufSerelizer);
		byte[] a = new byte[bufSerelizer.writerIndex()];
		System.arraycopy(bufSerelizer.array(), 0, a, 0, bufSerelizer.writerIndex());
		bufSerelizer.release();
		return a;
	}
}

