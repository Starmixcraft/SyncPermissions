package de.starmixcraft.syncpermissions.bungee.permissions;

import java.util.ArrayList;

import lombok.Data;
@Data
public class PermissionsGroup {
	private String name;
	private boolean defaultgroup;
	private boolean admin;
	private ArrayList<String> permissions;
	private ArrayList<String> heritages;
	private transient ArrayList<String> permissionsWithHeritages;
	private transient boolean loaded;
	
	public PermissionsGroup(String name, boolean defaultgroup, boolean admin, ArrayList<String> permissions,
			ArrayList<String> heritages) {
		this.name = name;
		this.defaultgroup = defaultgroup;
		this.admin = admin;
		this.permissions = permissions;
		this.heritages = heritages;
	}
	
}
