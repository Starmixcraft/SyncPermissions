package de.starmixcraft.syncpermissions.bungee.permissions;

import java.util.ArrayList;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Data;
@AllArgsConstructor
@Data
public class DataBasePlayerData {
	private UUID player;
	private String group;
	private ArrayList<String> permissions;
}
